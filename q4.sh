#!/bin/bash
cat $1 | crontab > /dev/null 2<&1
if [ $? -eq 0 ]
then
    echo Yes
else
    echo No
fi
