#!/bin/bash
read var
flag=0
for (( i=0;i<1000;i++ ))
do
      hi=`perl -MList::Util=shuffle -F'' -lane 'print shuffle @F' <<< $var`
      command -v $hi > /dev/null
      ret=$?
      if [ $ret -eq 0 ]     
      then
            echo Yes 
            echo $hi
            man $hi | col -b | grep -A1 "^\s*-"
            flag=1;
           break
       fi
       done
if [ $flag -ne 1 ]
then 
   echo No
fi 
