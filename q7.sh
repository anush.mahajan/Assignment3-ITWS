#!/bin/bash
read word
word1=${word^^}
var=$(echo "${word^^}" | rev )
if [[ $var == $word1 ]]
then
   echo "The entered string is a Palindrome"
else
    echo "The entered string is not a Palindrome"
fi
