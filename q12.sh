#!/bin/bash
echo Enter a Date
read var
echo Choose a format number :
echo "1)DD-MM-YYYY"
echo "2)MM/DD/YYYY"
read num
if [ $num -eq 1 ]
then
   date -d "$var" +%d-%m-%Y
else
   date -d "$var" +%m/%d/%Y
fi
